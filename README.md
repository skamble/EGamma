# CMSPODAS23
This repo is for the EGamma long exercise of the CMS Physics object & data analysis school [CMSPODAS2023](https://indico.desy.de/event/38207). 

Some references:
 - [CMSDAS CERN 2023](https://indico.cern.ch/event/1257234/)
 - [CMSDAS LPC 2023](https://twiki.cern.ch/twiki/bin/view/CMS/SWGuideCMSDataAnalysisSchoolLPC2023EGammaShortExercise)
 - [CMSDAS 2021](https://twiki.cern.ch/twiki/bin/view/CMS/SWGuideCMSDataAnalysisSchoolLPC2021EGammaExercise)
 - [LPC HATS 2020](https://twiki.cern.ch/twiki/bin/view/CMS/EGammaHATSatLPC2020)
 - [CMSDAS 2020](https://twiki.cern.ch/twiki/bin/view/CMS/SWGuideCMSDataAnalysisSchoolLPC2020EGammaExercise)


The exercise focuses on these following steps:

### Swan setup 
We will use here https://swan.cern.ch for some exercises involving Jupyter notebooks.
Please use software stack "102", maximum cores and maximum RAM, when selecting the environment.

![](reco_and_energy_corr/Pics/instr1.png)

Then run the commands below in the terminal:

>`cd SWAN_projects/CMSPODAS_HH_23/`

>`git clone https://gitlab.cern.ch/cms-podas23/pog/EGamma.git`

Now, go back to SWAN tab, and open the 'EGamma' folder.

### Energy calibration from ECAL
A brief introduction and exercise in ``reco_and_energy_corr/exercise-1.ipynb``<br>

### Reconstruction
A detailed introduction in slides

### Energy regression and correction
A brief introduction in slides and Jupyter notebook<br>
Exercises in ``reco_and_energy_corr/exercise-2.ipynb`` (skip) and ``reco_and_energy_corr/exercise-3.ipynb``<br>

### Identification
A brief introduction in slides and Jupyter notebook<br>
Exercises in ``egamma_id/exercise-4.ipynb``<br>

### Tag& Probe method
A brief introduction in slides and Jupyter notebook<br>
Exercise in ``Tnp/exercise-5.ipynb``<br>

### MVA ID training
A brief introduction in slides and Jupyter notebook<br>
Exercise in ``MVA/ElectronMVATutorial.ipynb``<br>

### Produce ntuples (Skip)
Based on the [NANOAOD framework](https://github.com/cms-nanoAOD/nanoAOD-tools.git). CMSSW environment and packages should be set up firstly as follows:<br>

``cmsrel CMSSW_10_6_29``<br>
``cd CMSSW_10_6_29/src``<br>
``cmsenv``<br>
``git clone https://github.com/cms-nanoAOD/nanoAOD-tools.git PhysicsTools/NanoAODTools``<br>
``git clone https://gitlab.cern.ch/cms-podas23/pog/EGamma.git``<br>
``rm -r EGamma/EgammaTools # needed in other exercises, but interferes with the build here``<br>
``scram b -j 12``<br>
``cd EGamma/ntuples``<br>

Ntuple tree contains electron branches could be produced by following the README.md therein.<br>
